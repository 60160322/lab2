module.exports = {
    isUserNameValid: function(username) {
        if (username.length < 3 || username.length > 15) {
            return false;
        }
        if (username.toLowerCase() !== username) {
            return false;
        }

        return true;
    },
    isAgeValid: function(age) {
        aa = parseInt(age);
        if (!(age.match(/^-{0,1}\d+$/))) { //เช็คว่าเป็นตัวเลขไหม
            return false;
        }
        if (aa < 18 || aa > 100) {
            return false;

        }

        return true;

    },
    isPasswordValid: function(password) {
        if (password.length < 8) {
            return false;
        }
        if (password.toLowerCase == password) {
            return false;
        }
        var i;
        var a = 0;
        for (i = 0; i < password.length; i++) {
            b = password.charAt(i);
            if ((b.match(/^-{0,1}\d+$/))) {
                a += 1;
            }
        }
        if (a < 2) {
            return false;
        }
        var d = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
        if (d.test(password) < 1) {
            return false;
        }

        return true;
    },
    isDateValid: function(day, month, year) {
        if (day < 1 || day > 31) {
            return false;
        }
        if (month < 1 || month > 12) {
            return false;
        }
        if (year < 1970 || year > 2020) {
            return false;
        }
        if (month = 1, 3, 5, 7, 8, 10, 12 && day > 32) {
            return false;
        }
        if (month = 4, 6, 9, 11 && day > 30) {
            return false;
        }
        var y1 = 400;
        var y2 = 100;
        if (month = 2) {
            if ((year % y1 && year % y2) == 0 && day > 29) {
                return false;
            }
        }


        return true;
    }
}