const chai = require('chai');
const expect = chai.expect;
const validate = require('./validate');

describe('Validate Model', () => {
    context('Function isUserNameValid', () => {
        it('Function prototype : boolean isUserNameValid(username: String)', () => {
            expect(validate.isUserNameValid('som')).to.be.true;
        });
        it('จำนวนตัวอักษรอย่างน้อย 3 ตัวอักษร', () => {
            expect(validate.isUserNameValid('su')).to.be.false;

        });
        it('ทุกตัวต้องเป็นตัวเล็ก', () => {
            expect(validate.isUserNameValid('Som')).to.be.false;
            expect(validate.isUserNameValid('soM')).to.be.false;

        });
        it('จำนวนตัวอักษรที่มากที่สุดคือ 15 ตัวอักษร', () => {
            expect(validate.isUserNameValid('som123456789012')).to.be.true;
            expect(validate.isUserNameValid('som1234567890123')).to.be.false;
        })
    });
    context('Function isAgeValid', () => {
        it('Function prototype : boolean isAgeValid (age: String)', () => {
            expect(validate.isAgeValid('18')).to.be.true;
        });
        it('age ต้องเป็นข้อความที่เป็นตัวเลข', () => {
            expect(validate.isAgeValid('a')).to.be.false;
        });
        it('อายุต้องไม่ต่ำกว่า 18 ปี และไม่เกิน 100 ปี', () => {
            expect(validate.isAgeValid('17')).to.be.false;
            expect(validate.isAgeValid('18')).to.be.true;
            expect(validate.isAgeValid('19')).to.be.true;
            expect(validate.isAgeValid('101')).to.be.false;
        });
    });
    context('Function isPasswordValid ', () => {
        it('Function prototype : boolean isUserNameValid(password: String)', () => {
            expect(validate.isPasswordValid('Som-0888888888')).to.be.true;
        });
        it('จำนวนตัวอักษรอย่างน้อย 8 ตัวอักษร', () => {
            expect(validate.isPasswordValid('Som-08')).to.be.false;

        });
        it('ต้องมีอักษรตัวใหญ่เป็นส่วนประกอบอย่างน้อย 1 ตัว', () => {
            expect(validate.isPasswordValid('som-888')).to.be.false;

        });
        it('ต้องมีตัวเลขเป็นส่วนประกอบอย่างน้อย 3 ตัว', () => {
            expect(validate.isPasswordValid('Som-suu8')).to.be.false;
            expect(validate.isPasswordValid('Som-s008')).to.be.true;
        });
        it('ต้องมีอักขระ พิเศษ !@#$%^&*()_+|~-=\`{}[]:";<>?,./ อย่างน้อย 1 ตัว', () => {
            expect(validate.isPasswordValid('Somms558')).to.be.false;
        });
    });
    context('Function isDateValid', () => {
        it('Function prototype : boolean isDateValid(day: Integer, month: Integer, year: Integer)', () => {
            expect(validate.isDateValid('7', '05', '1999')).to.be.true;
        });
        it('day เริ่ม 1 และไม่เกิน 31 ในทุก ๆ เดือน', () => {
            expect(validate.isDateValid('0', '01', '1999')).to.be.false;
            expect(validate.isDateValid('32', '01', '1999')).to.be.false;
        });
        it('month เริ่มจาก 1 และไม่เกิน 12 ในทุก ๆ เดือน', () => {
            expect(validate.isDateValid('1', '0', '1999')).to.be.false;
            expect(validate.isDateValid('1', '13', '1999')).to.be.false;
        });
        it('year จะต้องไม่ต่ำกว่า 1970 และ ไม่เกิน ปี 2020', () => {
            expect(validate.isDateValid('1', '1', '1969')).to.be.false;
            expect(validate.isDateValid('1', '11', '2021')).to.be.false;
        });
        it('เดือนแต่ละเดือนมีจำนวนวันต่างกัน', () => {
            expect(validate.isDateValid('32', '12', '1970')).to.be.false;
            expect(validate.isDateValid('31', '6', '2020')).to.be.false;
            expect(validate.isDateValid('29', '2', '1900')).to.be.false;
            expect(validate.isDateValid('29', '2', '2000')).to.be.true;
        });

    });
});